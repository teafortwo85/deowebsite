module.exports = function(grunt) {
	"use strict";

	// DEFINE PROJECTS HERE
	// [
	// 	project name,
	// 	project sass files,
	// 	project output destination,
	// 	watch files or specific file,
	// 	output style,
	// ]
	// ================================================

	var projectList = [

		// DEO Website
		[
			'deo',
			'src/scss/',
			'dist/',
			'**/*.css',
			'nested'
		]
	];

	// ================================================
	// Avoid editing below this line
	// ================================================

	// Starter lists
	var sassList =         [];
	var regexList =        [];
	var postList =         [];
	var watchList =        [];
	var watchStagingList = [];
	var watchDevList =     [];
	var buildList =        [];

	// Build project list
	projectList.forEach(function(project) {

		// Create staging sass task
		sassList[project[0]] = {
			options: {
				outputStyle: project[4],
				lineNumbers: false,
				sourceMap: false,
				sourceComments: false
			},
			files: [{
				expand: true,
				cwd: project[1],
				src: ['**/*.scss'],
				dest: project[2],
				ext: '.css'
			}]
		};

		// Create dev sass task
		sassList[project[0] + '-dev'] = {
			options: {
				outputStyle: project[4],
				lineNumbers: false,
				sourceMap: false,
				sourceComments: true
			},
			files: [{
				expand: true,
				cwd: project[1],
				src: ['**/*.scss'],
				dest: project[2],
				ext: '.css'
			}]
		};

		// Create variable convert regex task
		regexList[project[0] + '-convert'] = {
			actions: [
				{
					name: 'convert',
					search: '[\$][\{](.*?)[\}]',
					replace: '$($1)',
					flags: 'g'
				}
			],
			files: [{
				expand: true,
				cwd: project[2],
				src: [project[3]],
				dest: project[2],
				ext: '.css'
			}]
		};

		// Create variable revert regex task
		regexList[project[0] + '-revert'] = {
			actions: [
				{
					name: 'revert',
					search: '[\$][\()](.*?)[\)]',
					replace: '${$1}',
					flags: 'g'
				}
			],
			files: [{
				expand: true,
				cwd: project[2],
				src: [project[3]],
				dest: project[2],
				ext: '.css'
			}]
		};

		// Create post(autoprefixer) css task
		postList[project[0]] = {
			options: {
				map: false,
				processors: [
					require('autoprefixer-core')({
						browsers: ['chrome >= 33', 'safari >= 6.1', 'firefox >= 26', 'ie >= 9']
					}).postcss
				]
			},
			files: [{
				expand: true,
				cwd: project[2],
				src: [project[3]],
				dest: project[2],
				ext: '.css'
			}]
		};

		// Create staging watch task
		watchList['css-' + project[0]] = {
			options: {
				atBegin: true
			},
			files: [project[1]+'**/*.scss'],
			tasks: ['cssBuild:'+project[0]]
		};

		// Create dev watch task
		watchList['css-' + project[0] + '-dev'] = {
			options: {
				atBegin: true
			},
			files: [project[1]+'**/*.scss'],
			tasks: ['cssBuild:'+project[0]+'-dev']
		};

		// Create staging watch concurrent list
		watchStagingList.push('watch:css-'+project[0]);

		// Create dev watch concurrent list
		watchDevList.push('watch:css-'+project[0]+'-dev');

		// Register staging build tasks
		grunt.registerTask('cssBuild:'+project[0], [
			'sass:'+project[0],
			'regex-replace:'+project[0]+'-convert',
			'postcss:'+project[0],
			'regex-replace:'+project[0]+'-revert'
		]);

		// Register dev build tasks
		grunt.registerTask('cssBuild:'+project[0]+'-dev', [
			'sass:'+project[0]+'-dev',
			'regex-replace:'+project[0]+'-convert',
			'postcss:'+project[0],
			'regex-replace:'+project[0]+'-revert'
		]);
	});

	// Load Tasks
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-regex-replace');
	grunt.loadNpmTasks('grunt-regex-replace');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Grunt Init
	grunt.initConfig({
		'sass': sassList,
		'regex-replace': regexList,
		'postcss': postList,
		'watch': watchList,
		'concurrent': {
			options: { logConcurrentOutput: true },
			'css-watch-staging': { tasks: watchStagingList },
			'css-watch-dev': { tasks: watchDevList }
		}
	});

	// Staging Build Task
	grunt.registerTask('cssBuild', function() {
		projectList.forEach(function(project) {
			grunt.task.run([
				'cssBuild:'+project[0]
			]);
		})
	});

	// Dev Build Task
	grunt.registerTask('cssBuild:dev', function() {
		projectList.forEach(function(project) {
			grunt.task.run([
				'cssBuild:'+project[0]+'-dev'
			]);
		})
	});

	// Staging Watch Task
	grunt.registerTask('cssWatch', function() {
		grunt.task.run(['concurrent:css-watch-staging']);
	});

	// Dev Watch Task
	grunt.registerTask('cssWatch:dev', function() {
		grunt.task.run(['concurrent:css-watch-dev']);
	});

	grunt.registerTask('cssHelp', function() {
		grunt.log.subhead('Build All Projects (staging)'['cyan']);
		grunt.log.writeln('grunt cssBuild');
		grunt.log.subhead('Build All Projects (dev)'['cyan']);
		grunt.log.writeln('grunt cssBuild:dev');
		grunt.log.subhead('Watch All Projects (staging)'['cyan']);
		grunt.log.writeln('grunt cssWatch');
		grunt.log.subhead('Watch All Projects (dev)'['cyan']);
		grunt.log.writeln('grunt cssWatch:dev');
		grunt.log.subhead('Individual Build (staging)'['cyan']);
		projectList.forEach(function(project) {
			grunt.log.writeln('grunt cssBuild:'+project[0]);
		});
		grunt.log.subhead('Individual Build (dev)'['cyan']);
		projectList.forEach(function(project) {
			grunt.log.writeln('grunt cssBuild:'+project[0]+'-dev');
		});
		grunt.log.subhead('Individual Watch (staging)'['cyan']);
		projectList.forEach(function(project) {
			grunt.log.writeln('grunt watch:css-'+project[0]);
		});
		grunt.log.subhead('Individual Watch (dev)'['cyan']);
		projectList.forEach(function(project) {
			grunt.log.writeln('grunt watch:css-'+project[0]+'-dev');
		});
	});
};